package com.deveducation.myandroidconverter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Spinner spinnerFrom;
    private Spinner spinnerTo;
    private Button btn;
    private TextView result;
    private TextView input;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        btn = (Button) findViewById(R.id.btnConvert);
        result = (TextView) findViewById(R.id.textResult);
        input = (TextView) findViewById(R.id.editTextNumberDecimal);

        spinnerFrom = (Spinner) findViewById(R.id.spinnerFrom);
        ArrayAdapter<String> adapterFrom = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, Calc.getItems());
        adapterFrom.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerFrom.setAdapter(adapterFrom);

        spinnerTo = (Spinner) findViewById(R.id.spinnerTo);
        ArrayAdapter<String>adapterTo = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, Calc.getItems());
        adapterTo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTo.setAdapter(adapterTo);

        btn.setOnClickListener(handleClick);


    }

    private View.OnClickListener handleClick = new View.OnClickListener(){
        public void onClick(View arg0) {
            Button btn = (Button) arg0;
            result.setText(String.valueOf(
                    Calc.convertLength(Double.parseDouble(input.getText().toString()),
                            spinnerFrom.getSelectedItemPosition(),
                            spinnerTo.getSelectedItemPosition())));

        }
    };


}