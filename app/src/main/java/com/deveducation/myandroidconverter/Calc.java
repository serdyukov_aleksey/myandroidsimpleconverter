package com.deveducation.myandroidconverter;

import java.util.ArrayList;
import java.util.List;

public class Calc {

    public static List<String> getItems (){
        List<String> items = new ArrayList<String>();
        items.add("meter");
        items.add("miles");
        items.add("yards");
        items.add("verst");
        return items;
    }

    public static double convertLength (double value, int indexFrom, int indexTo){
        double coef[] = {1, 0.000621, 1.09361, 0.000937383};
        return value / coef[indexFrom] * coef[indexTo];
    }


}
